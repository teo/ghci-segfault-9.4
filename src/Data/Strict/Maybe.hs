module Data.Strict.Maybe where

import Prelude (Show)

data Maybe a = Nothing | Just !a
  deriving (Show)
