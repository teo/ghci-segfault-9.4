module MyLib where
import qualified Data.Strict.Maybe as Strict

data StrictMaybe a = StrictJust !a | StrictNothing
  deriving Show

data Foo = 
  Foo 
    !(Strict.Maybe Int) 
    !(Strict.Maybe Int) 
  deriving Show

mkFoo :: Foo
mkFoo = Foo Strict.Nothing Strict.Nothing

data Bar = Bar !Int !Foo
  deriving Show
