module Main (main) where

import MyLib

mkBar _ = Bar 0 mkFoo

main :: IO ()
main = print $ mkBar ()
